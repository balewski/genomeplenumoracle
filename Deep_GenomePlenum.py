import os, time, sys
import socket  # for hostname
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
from tensorflow.keras.layers import Dense, maximum, Input, Dropout, Conv1D,MaxPool1D,Flatten,LeakyReLU
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint , ReduceLROnPlateau

from Util_Func import write_yaml, read_yaml, read_oneGenome_fasta, genomePlenum_inputGeneratorFnc2 , procure_ocean

import numpy as np
import h5py
import random


# this sepecail keras layer:
sys.path.append(os.path.abspath("/global/homes/b/balewski/digitalMind/keras/superkeras-chanchana/"))
from permutational_layer import PairwiseModel, PermutationalEncoder,PermutationalLayer, move_item
from layers import repeat_layers

print('deep-libs imported elaT=%.1f sec'%(time.time() - start))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
from tensorflow.python.keras.callbacks import Callback
import tensorflow.keras.backend as K
import tensorflow as tf
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]   
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        #lr = K.eval(optimizer.lr * (1. / (1. + optimizer.decay * optimizer.iterations)))
        lr = K.eval(optimizer.lr)
        self.hir.append(lr)


#............................
#............................
#............................
class Deep_GenomePlenum(object):
    #If you want multiple, independent "constructors", you can provide these as class methods. 

    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)
        print(self.__class__.__name__,'TF ver:', tf.__version__, 'prj:',self.prjName)
        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)
        self.metaF=self.dataPath+'/meta.'+self.prjName+'.yaml'
        #metaF=self.dataPath+'/meta.'+self.prjName+'.yaml'

    # alternative constructors
    @classmethod #........................
    def formater(cls, args):
        print('Cnst:formater')
        obj=cls(**vars(args))
        contentF=args.dataPath+'/content.'+args.prjName+'.yaml'
        obj.metaD=read_yaml(contentF)
        return obj

    @classmethod #........................
    def predictor(cls, args):
        print('Cnst:sim-pred')
        obj=cls(**vars(args))
        obj.events=0
        obj.read_metaInp(obj.metaF)
        obj.read_hyperInp()
        return obj


    @classmethod #........................
    def trainer(cls, args):
        print('Cnst:trainer, host=',socket.gethostname())
        config = tf.ConfigProto(intra_op_parallelism_threads=0, 
                                inter_op_parallelism_threads=1, # independent in your TensorFlow graph
                                allow_soft_placement=True)

        session = tf.Session(config=config)
        if args.nCpu>0:
            tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
            print('M: restrict CPU count to ',args.nCpu)

        obj=cls(**vars(args))
        obj.read_metaInp(obj.metaF)
        obj.read_hyperInp()
        obj.train_hirD={'acc': [],'loss': [],'lr': [],'val_acc': [],'val_loss': []}
        # overwrite some of hpar if provided from command line
        if obj.dropFrac!=None:
            obj.hparams['dropFrac']=obj.dropFrac
        # sanity checks
        assert obj.hparams['dropFrac']>=0
        assert obj.hparams['steps']>5

        return obj
 
#............................
    def import_all_fasta(self):
        rawPath=self.metaD['rawPath']
        aminoacidSet=set(self.metaD['aminoacids'])
        myShuffle=self.metaD['fastaShuffle']
        print('import_all_fasta from:',rawPath, 'aminoacids=',self.metaD['aminoacids'])
        start=time.time()
        fastaNL=os.listdir(rawPath)
        print('found %d fasta files'%len(fastaNL),', elaT=%.1f sec'%(time.time() - start))
        if myShuffle:  random.shuffle(fastaNL)
        if self.events>0 :
            fastaNL=fastaNL[:self.events]
            print('reduce number of genomes to ',len(fastaNL))
        genLenLo, genLenHi=self.metaD['genLenClip']
        print('some genome names:',fastaNL[2:5],'genLen hi/lo=',genLenLo, genLenHi)
        genomeDB={}
        nProTot=0
        cntPro={'lo':0,'acc':0,'hi':0,'any':0}
        cntGen={'lo':0,'acc':0,'hi':0,'any':0}

        start=time.time()
        for name in fastaNL:
            assert '.faa' in name
            fName=rawPath+name
            g,c=read_oneGenome_fasta(fName,aminoacidSet, self.metaD['protLenClip'],myShuffle=myShuffle ) # maxProt=600)
            cntGen['any']+=1
            nPro=len(g)
            if nPro < genLenLo:
                cntGen['lo']+=1  # and skip it
            elif nPro > genLenHi:
                cntGen['hi']+=1  # and skip it
            else:  # use it 
                cntGen['acc']+=1
                nProTot+=nPro
                for x in cntPro: cntPro[x]+=c[x]
                genomeDB[name[:-4]]=g
                nGenome=len(genomeDB)
            if cntGen['any']%100==0 : 
                print('%d genomes in, last=%s'%(nGenome,name),c)
        
        print('found %d genomes , avr=%.1f proteins/genome'%(nGenome, nProTot/nGenome),', elaT=%.1f sec'%(time.time() - start), '\n genome cnt=',cntGen,'\n protein cnt=',cntPro) 

        return genomeDB



#...!...!....................
    def split_genomes(self,genomeDB):
        nGenTot=len(genomeDB)
        start=time.time()
        print('split_genomes , totLen',nGenTot)
        numSegm=self.metaD['numSegm']
        print('split of frames started ...')

        out={}
        cnt={'inp':0}

        print('split traces K-fold=',numSegm)
        for seg in range(numSegm):
            out[seg]=[]
    
        for gName in genomeDB:
            cnt['inp']+=1
            #... assign segment
            sg=np.random.randint(numSegm)
            out[sg].append(gName)
        print('  achieved split for  %d frames to %d segments:'%(nGenTot,numSegm), [ (sg,len(out[sg])) for sg in out ],', elaT=%.1f sec'%(time.time() - start))

        self.metaD['splitCnt']=[ len(out[sg]) for sg in range(numSegm)]

        '''
        # procure oceansby sampling ~10% of proteins from every segment separately
        oceanD={}
        for seg in range(numSegm):
            oneOcean=self.procure_ocean(out[seg],genomeDB)
            oceanN='ocean%d'%seg
            # attache ocean to both
            out[seg].append(oceanN)
            genomeDB[oceanN]=oneOcean
        '''
        return out

#...!...!....................
    def save_fasta_hdf5(self,genomeDB,splitD):
        # https://www.pythonforthelab.com/blog/how-to-use-hdf5-files-in-python/
        # use of group/data set for genome/[seq,protName,protLen,etc]
        print('START save_fasta_hdf5 numSeg=',len(splitD))
        start=time.time()
        dt_str = h5py.special_dtype(vlen=str)
        kPr=100 # printing frequency

        for sg in splitD:
            genomeL=splitD[sg]
            outF=self.dataPath+'/genomePlen_seg%d.hd5'%sg
                
            h5f = h5py.File(outF, 'w')
            #print('SF sg',sg, genomeL)
            for k, gName in enumerate(genomeL):
                oneGen= genomeDB[gName]
                numProt=len(oneGen)
                if k%kPr==0: print('%d genome of %d, numProt=%d gName=%s'%(k, len(genomeL),numProt,gName ))
                dgrp=h5f.create_group(gName) # for each genome

                ds4=dgrp.create_dataset('amino_seq', (numProt,), dtype=dt_str)
                ds2=dgrp.create_dataset('amino_len', (numProt,),dtype='int32')
                ds3=dgrp.create_dataset('fasta_idx', (numProt,),dtype='int32')
                ds1=dgrp.create_dataset('prot_name', (numProt,), dtype=dt_str)

                for i,[protN, seqLen, fastaIdx, seq] in enumerate(oneGen):
                    ds1[i]=protN
                    ds2[i]=seqLen
                    ds3[i]=fastaIdx
                    ds4[i]=seq
                    if i<5 and k%kPr==0: print('   ',i, seq[:20]+'...', seqLen,protN)

            if sg<3 :
                print('save data as hdf5:',outF)
            h5f.close()
            xx=os.path.getsize(outF)/1048576
            print('closed  hdf5:',outF,' size=%.2f MB, genomes=%d'%(xx,len(genomeL)),' , elaT=%.1f sec'%(time.time() - start))
        print('save_fasta_hdf5 completed, elaT=%.1f sec'%(time.time() - start))

#...!...!....................
    def read_oneSeg_hdf5(self,inpF):  
        # https://www.pythonforthelab.com/blog/how-to-use-hdf5-files-in-python/
        # use of group/data set for genome/[seq,protName,protLen,etc]
        start=time.time()
        h5f = h5py.File(inpF, 'r')
        objD={}
        for gName in h5f.keys():
            objD[gName]={}
            for dsn in ['amino_seq','amino_len']: # skip:'prot_name','fasta_idx'
                x=gName+'/'+dsn
                #print(gName,'x',x)
                obj=h5f[x][:]
                #print('read ',x,obj.shape)
                objD[gName][dsn]=obj
        h5f.close()
        return objD

#...!...!....................
    def read_metaInp(self,metaF):
        self.metaD=read_yaml(metaF)
        print('read metaInp:',self.metaD)
        self.numSegm=self.metaD['numSegm']  #  K-fold partition of scaffolds
        self.prjName2='%s-%s'%(self.prjName,self.modelDesign)
        self.hparF='./hpar_'+self.prjName2+'.yaml'
        if self.algoIdx : 
            self.prjName2+='-algo%d'%self.algoIdx
        self.data={}  # [train/val][genome[]/ocean]

        # prepare 1-hot base for aminoacids:
        aminoacidL=self.metaD['aminoacids']
        myDim=len(aminoacidL)
        padChar=self.metaD['padChar']
        self.oneHotBase={} # len=myDim, pad=00000000...
        i=0
        for x in aminoacidL:
            self.oneHotBase[x]=np.eye(myDim)[i]
            i+=1
        self.oneHotBase[padChar]=np.zeros(myDim)
        print('oneHot base size=',len(self.oneHotBase), 'sample:')
        for x in [padChar, 'A', 'C','Y']:
            print('base:',x,'1-hot:',self.oneHotBase[x].tolist())

#...!...!....................
    def read_hyperInp(self): # . . . load hyperparameters        
        bulk=read_yaml(self.hparF)
        print('hpar bulk',bulk)
        self.hparams=bulk

        # overwrite some of hpar if provided from command line
        if self.batch_size!=None:
            self.hparams['batch_size']=self.batch_size

        if self.steps!=None:
            self.hparams['steps']=self.steps

#...!...!....................
    def load_segmented_input(self,dom):
        start=time.time()

        numUsedSegm=self.numSegm-1  # the last 1 segments  reserved for end-testing
        numTrainSegm=numUsedSegm-1
        print('\nload_segmented_input for dom:',dom, ',num used segments:',numUsedSegm)
        assert numUsedSegm>=2  # makes no sense to split to train/eval/test
        assert len(self.metaD['splitCnt'])== self.metaD['numSegm'] # in case you edit metaD - a rare case

        if dom=='val':
            kOff=self.kfoldOffset
            jL=(kOff+numTrainSegm)%numUsedSegm # one element                
            kL=[jL]

        if dom=='test':
            jL=numUsedSegm # one element at the end
            kL=[jL]

        if dom=='train':
            kOff=self.kfoldOffset
            kL=[ (kOff+j)%numUsedSegm for j in range(numTrainSegm)]


        print('segL:',kL,' dom:',dom)
        assert len(kL)>0

        #print('metaAll:',self.metaD)
        splitCnt=np.array(self.metaD['splitCnt'])

        # adjust the size of inpit but only on units of segments
        if self.events>0 and len(kL)>1:
            print('initial seg list kL=',kL)
            i=0
            sumEve=0
            for k in kL:
                sumEve+=splitCnt[k].sum()
                i+=1
                if self.events< sumEve:
                    print('skip segments >%d, nEve=%d - it is more than needed'%(k,sumEve))
                    break
            kL=kL[:i]
            print('final seg list kL=',kL)

        splitCntUsed=splitCnt[kL] # now only those to be loaded are present
        num_eve=splitCntUsed.sum()
        print('segL cntA:',splitCntUsed,'target sum=',num_eve)
        
        rawD={}        
        for k in kL:
            inpF1=self.dataPath+'/genomePlen_seg%d.hd5'%k
            print('read data from hdf5:',inpF1)
            h5f1 = h5py.File(inpF1, 'r')
            #print('see keys1', [x for x in h5f1.keys()])
            #assert 'ocean%d'%k in  h5f1.keys()
            for gName in h5f1.keys():
                if 'ocean' in gName: continue # skip, tmp
                allProtN=gName+'/amino_seq'
                #print('hh',gName,allProtN)
                protL=h5f1[allProtN][:]
                #print('ttt',type(protL),protL.shape)                
                rawD[gName]=protL
            print('  done w/ segment k=%d, sum: numGenome=%d '%(k,len(rawD)))
        assert len(rawD) >0
        
        ocean=procure_ocean(rawD)
        self.data[dom]={'genome':rawD,'ocean':ocean}


#...!...!..................
    def build_permu_model(self,one_shape,num_inputs ):
        hpar=self.hparams
        print('build_model hpar:',hpar)
        dropFrac=self.hparams['dropFrac']
        print('\nPER step-A - - - - - - - -')
        print('PER: 1 one_shape=',one_shape,' num_inputs=',num_inputs,'dropFrac=%.2f'%dropFrac)
        model1 = PairwiseModel(
                one_shape, repeat_layers(Conv1D, [32], [3], name="pairCNN1D"), name="pairwise_op"
            )
        # this is a hack, I should better understand how PairwiseModel(.) works
        h=model1.layers[-1].output # the last layer
        k=1
        cnn_ker=hpar['conv_kernel']
        # .....  CNN-1D layers, was 'relu'
        for dim in hpar['conv_filter']:
            k+=1
            h= Conv1D(dim,cnn_ker,activation='linear',padding='valid' ,name='cnn%d_d%d_k%d'%(k,dim,cnn_ker))(h)
            h =LeakyReLU(name='act%d'%(k))(h)

            h= MaxPool1D(pool_size=hpar['pool_len'], name='pool_%d'%(k))(h)
            print('cnn',k,h.get_shape())
 
        h=Flatten(name='to_1d0')(h)
        pairwise_op = Model(model1.inputs, outputs=h)
        pairwiseSize=pairwise_op.count_params()
        mod=pairwise_op        
        print('PER: PairOP+JanPool  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))
        mod.summary()

        if self.verb>1:
            outF='pairwise_op.graph.svg'
            plot_model(mod, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M: saved1 ',outF)

        print('\nPER step-B - - - - - - - ')
        encoderRow_op = PermutationalEncoder(pairwise_op,num_inputs , name="encoder_row",encode_identical=False) # skip auto-correlation terms

        mod=encoderRow_op
        print('PER: EncoderRow  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if self.verb>2:
            mod.summary()
            outF='encoderRow_op.graph.svg'
            plot_model(encoderRow_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved2 ',outF)


        print('\nPER step-C - - - - - - ')
        perm_layer = PermutationalLayer(encoderRow_op, name="permutational_layer", pooling=maximum)

        mod=perm_layer
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if self.verb>2:
            mod.summary()
            outF='permLyr.graph.svg'
            plot_model(perm_layer, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved3 ',outF)

        print('\nPER step-D - - - - - - \n add few common FC layers --> sigmoid')
        # append non-permutational  layers to the model
        model2=perm_layer
        h=model2.layers[-1].output # the last layer

        print('in FC comm=>',h.get_shape())
        # .... FC  layers   
        for i,dim in enumerate(hpar['fc_dims']):
            h = Dense(dim,activation='linear',name='fc%d'%i)(h)
            h =LeakyReLU(name='acb%d'%(i))(h)
            if dropFrac>0:  h = Dropout(dropFrac,name='drop%d'%i)(h)

        outputs= Dense(1, activation='sigmoid',name='sigmoid')(h)
        model3 = Model(model2.inputs, outputs)

        mod=model3
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if self.verb>0:
            mod.summary()
            outF='fullModel.graph.svg'
            plot_model(model2, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved4 ',outF)
        self.model=model3

        # create summary record
        rec={'hyperParams':self.hparams,
             'modelWeightCnt':int(self.model.count_params()),
             'modelLayerCnt':len(self.model.layers),
             'pairwiseWeightCnt' : pairwiseSize,
             'modelDesign' : self.modelDesign,
             'modelSize'   : self.model.count_params(),
             'hostName'    : socket.gethostname(),
             'kfoldOffset' : self.kfoldOffset,
             'algoIdx'     : self.algoIdx,
             'slurmJID'    : os.getenv('SLURM_JOBID','interactive')
        }
        self.sumRec=rec
        
#...!...!..................

    #............................
    def compile_model(self): 
        print('compile_model: loss=',self.hparams['lossName'],' optName=',self.hparams['optimizerName'])
        self.model.compile(optimizer=self.hparams['optimizerName'], loss=self.hparams['lossName'], metrics=['accuracy'])

        print('model size=%.1fK compiled elaT=%.1f sec'%(self.model.count_params()/1000.,time.time() - start))
        
        

    #............................
    def train_model(self,trainGen, valGen):
        callbacks_list = []
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        if self.earlyStopPatience>0:
            earlyStop=EarlyStopping(monitor='val_loss', patience=self.earlyStopPatience, verbose=1, min_delta=2.e-4, mode='auto')
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping, patience=',self.earlyStopPatience)
        

        if self.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.outPath+'/'+self.prjName2+'.weights_best.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=4)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint')

        if self.reduceLRPatience>0:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=self.hparams['reduceLR_factor'], patience=self.reduceLRPatience, min_lr=0.0, verbose=1,min_delta=0.003)
            callbacks_list.append(redu_lr)
            print('enabled ReduceLROnPlateau, patience=',self.reduceLRPatience,',factor=',self.hparams['reduceLR_factor'])


        steps=self.hparams['steps']
        print('\nTrain_model epochs=',self.epochs,' batch=',self.hparams['batch_size'], 'steps=',steps)
        startTm = time.time()
        model=self.model
        myCallbacks=callbacks_list

        hir=model.fit_generator(trainGen, callbacks=myCallbacks, epochs=self.epochs, steps_per_epoch=steps ,validation_data=valGen, validation_steps=steps//5, use_multiprocessing=True ) #, workers=4, max_queue_size=10)

        # for workers=0 I see 41 threads/training, for any other setting I see 48 threads
        # see this Your generator is NOT thread-safe. https://stackoverflow.com/questions/52932406/is-the-class-generator-inheriting-sequence-thread-safe-in-keras-tensorflow

        self.train_hirD=hir.history
        self.train_hirD['lr']=lrCb.hir

        totEpochs=len(self.train_hirD['loss'])
        earlyStopOccured= self.epochs != totEpochs
        
        #print('TgotC',self.epochs ,totEpochs , earlyStopOccured)
        #print('TgotA', self.train_hirD.keys())
        #print('TgotA2',lrCb.hir)

        #evaluate performance for the last epoch
        lossT=self.train_hirD['loss'][-1]
        lossV=self.train_hirD['val_loss'][-1]
        accV=self.train_hirD['val_acc'][-1]
 
        fitTime=time.time() - start
        print('\n End Validation  Loss:%.3f'%lossV,', fit time=%.1f sec'%(fitTime))
        self.train_sec=fitTime

        #... add info to summary
        rec=self.sumRec
        rec['fitMin']=fitTime/60.
        rec['totEpochs']=totEpochs
        rec['train_loss']=float(lossT)
        rec['val_loss']=float(lossV)
        rec['val_acc']=float(accV)
        rec['earlyStopOccured']=int( earlyStopOccured)

#...!...!..................
    def make_predictions(self,dom):
        inpGen = genomePlenum_inputGeneratorFnc2(self.data[dom],self)
        # test one of generators
        xx,yy =next(inpGen)  # X=[ np(batch,one_shape) ]_item
        
        steps=self.hparams['steps']
        YhotL=[]; YscoreL=[]
        for s in range(steps):
            print('do step=%d of %d'%(s,steps))
            xx,yy =next(inpGen)  
            scoreV=self.model.predict(xx)
            YhotL.append(yy)
            YscoreL.append(scoreV.flatten())
            #print('aa',yy.shape,scoreV,YscoreL[0])
            #ok99
        Yhot=np.concatenate(YhotL).ravel()
        Yscore=np.concatenate(YscoreL).ravel()
        print('Yhot,Yscore',Yhot.shape,Yscore.shape)
        return Yhot,Yscore

 
#...!...!..................
    def save_model_full(self):
        outF=self.outPath+'/'+self.prjName2+'.model.h5'
        print('save model full to',outF)
        self.model.save(outF)
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%xx)
 
    #............................
    def load_model_full(self,path=''):
        start = time.time()
        fname=path+'/'+self.prjName2+'.model.h5'
        print('load model from ',fname)
        model=load_model(fname) # creates model from HDF5
        if self.verb>2:
            self.model.summary()
        print('model loaded: inp=',len(model.input_shape),',out=',model.output_shape,', params=',model.count_params(),' layers=',len(model.layers),'elaT=%.2f sec'%(time.time() - start))
        self.model=model

    #............................
    def load_weights(self,name):
        start = time.time()
        outF5m=self.prjName2+'.%s.h5'%name
        print('load  weights  from',outF5m,end='... ')
        self.model.load_weights(outF5m) # creates mode from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))

