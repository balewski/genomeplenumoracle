import numpy as np

# for AUC of ROC
from sklearn.metrics import roc_curve, auc

from matplotlib import cm as cmap


from Plotter_Backbone import Plotter_Backbone

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_GenomePlenum(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)
        self.metaD=metaD


#............................
    def plot_model(self,deep,flag=0):
        #print('plot_model broken in TF=1.4, skip it'); return

        if 'cori' not in socket.gethostname(): return  # software not installed
        model=deep.model
        fname=self.outPath+'/'+deep.prjName+'_graph.svg'
        plot_model(model, to_file=fname, show_layer_names=flag>0, show_shapes=flag>1)
        print('Graph saved as ',fname,' flag=',flag)


#............................
    def plot_raw_fasta(self,recD,text,figId=4):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,4))
        nrow,ncol=2,1
        #print(recD)

        uG=[]
        uP=[]
        for gN in recD:
            genomeL=recD[gN]
            uG.append(len(genomeL)/1000.)
            for name,seqLen, fastaIdx,seqStr in genomeL:
                #print(name)
                #sl=len(seqStr)
                sl=seqLen
                uP.append( sl/1000.)

        ax=self.plt.subplot(nrow,ncol, 1)
        ax.hist(uG, 100,alpha=0.5)
        ax.set(xlabel= 'num proteins (k)' ,ylabel='genomes',title=text+', num data:%d'%len(uG))
        ax.grid(color='brown', linestyle='--')
        #ax.set_yscale('log')

        ax=self.plt.subplot(nrow,ncol, 2)
        ax.hist(uP, 100,alpha=0.5, range=(0,2))
        ax.set(xlabel= 'num amino-acid (k)' ,ylabel='proteins',title=text+', num data:%d'%len(uP))
        ax.grid(color='brown', linestyle='--')
        #ax.set_yscale('log')



# - - - - - OLD - - - - - - 
#............................
    def plot_frames(self,dataA,nFr,figId=7):
        figId=self.smart_append(figId)
        X=dataA['X']
        Y=dataA['Y']
        Aux=dataA['AUX']
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,6))
        
        nrow,ncol=2,4
        print('plot input for trace nFr=',nFr)
        j=0
        for it in range(nFr):
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1
            frame=X[:,it,:]
            #print(it,frame)
            P=Aux[it]
            lab=Y[it]
            #print('P=',P,frame.shape,lab)
            ax.scatter(P[0],P[1],c='r',marker='X', linewidth=0.1)
            xA=frame[:,0]
            yA=frame[:,1]
            ax.scatter(xA,yA,c='b', s=5) 
            
            ax.set_ylim(0,self.metaD['max_y'])
            ax.set_xlim(0,self.metaD['max_x'])
            ax.set_title('label=%d'%lab)


#............................
    def plot_train_history(self,deep,args,figId=10):
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(10,8.5))
        nrow,ncol=(3,3)
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (1,0), colspan=2 )
        ax2 = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=2,sharex=ax1 )
        ax3 = self.plt.subplot2grid((nrow,ncol), (2,0), colspan=2,sharex=ax1 )

        DL=deep.train_hirD
        val_acc=DL['val_acc'][-1]
        val_loss=DL['val_loss'][-1]

        tit1='%s, train %.1f min, end val_loss=%.3g'%(deep.prjName2,deep.train_sec/60.,val_loss)
        tit2='earlyStopOccured=%d, end val_acc=%.3f'%(deep.sumRec['earlyStopOccured'],val_acc)
        tit3='kFoldOff=%d, slurmJID=%s'%(deep.kfoldOffset,deep.sumRec['slurmJID'])


        ax1.set(ylabel='loss=%s'%deep.hparams['lossName'],title=tit1,xlabel='epochs')
        ax1.plot(DL['loss'],'.-.',label='train')
        ax1.plot(DL['val_loss'],'.-',label='valid')
        ax1.legend(loc='best')
        ax1.grid(color='brown', linestyle='--',which='both')

        ax2.set(xlabel='epochs',ylabel='accuracy',title=tit2)
        ax2.plot(DL['acc'],'.-',label='train')
        ax2.plot(DL['val_acc'],'.-',label='valid')
        ax2.legend(loc='bottom right')
        ax2.grid(color='brown', linestyle='--',which='both')

        ax3.plot(DL['lr'],'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate',xlabel='epochs',title=tit3)


#............................
    def plot_labeled_scores(self,Ytrue,Yscore,segName,score_thr=0.5,figId=21):
        if figId>0:
            self.figL.append(figId)
            fig=self.plt.figure(figId,facecolor='white', figsize=(7,4))
            ax=self.plt.subplot(1,1, 1)
        else:
            figId=-figId
            self.plt.figure(figId)
            nrow,ncol=3,3
            ax = self.plt.subplot(nrow, ncol, ncol)

        #print('nn',Yscore.ndim)
        assert Yscore.ndim==1
        u={0:[],1:[]}
        mAcc={0:0,1:0}
        for ygt,ysc in  zip(Ytrue,Yscore):
            u[ygt].append(ysc)
            if ysc> score_thr : mAcc[ygt]+=1

        mInp={0:len(u[0])+1e-3,1:len(u[1])+1e-3}

        print('Labeled scores found mAcc',mAcc, ' thr=',score_thr)

        bins = np.linspace(0.0, 1., 50)
        txt=''
        txt='FPR=%.2f, '%(mAcc[0]/mInp[0])
        ax.hist(u[0], bins, alpha=0.5,label=txt+'%d NEG/ %d'%(mAcc[0],mInp[0]))
        txt='TPR=%.2f, '%(mAcc[1]/mInp[1])
        ax.hist(u[1], bins, alpha=0.6,label=txt+'%d POS/ %d'%(mAcc[1],mInp[1]))

        ax.axvline(x=score_thr,linewidth=2, color='blue', linestyle='--')

        ax.set(xlabel='predicted score', ylabel='num samples')
        #ax.set_yscale('log')
        ax.grid(True)
        ax.set_title('Labeled scores dom=%s'%(segName))
        ax.legend(loc='upper right', title='score thr > %.2f'%score_thr)


#............................
    def plot_AUC(self,Yhot,Yprob,name,args,figId=20):
        if figId>0:
            self.figL.append(figId)
            fig=self.plt.figure(figId,facecolor='white', figsize=(5,8.5))
            nrow,ncol=2,1
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax1 = self.plt.subplot(nrow, ncol, 1)
            ax2 = self.plt.subplot(nrow, ncol, 2)

        else:
            self.plt.figure(-figId)
            nrow,ncol=3,3
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax1 = self.plt.subplot(nrow, ncol, nrow*ncol-ncol)
            ax2 = self.plt.subplot(nrow, ncol, nrow*ncol)


        # produce AUC of ROC
        '''
        With the Model class, you can use the predict method which will give you a vector of probabilities and then get the argmax of this vector (with np.argmax(y_pred1,axis=1)).
        '''
        # here I have only 1 class - so I can skip the armax step

        print('\nYprob',Yprob.shape,Yprob[:5])
        print('YHot (truth)',Yhot.shape,Yhot[:5])

        fpr, tpr, _ = roc_curve(Yhot,Yprob)
        roc_auc = auc(fpr, tpr)

        LRP=np.divide(tpr,fpr)
        fpr_cut=0.05
        for x,y in zip(fpr,LRP):
            if x <fpr_cut :continue
            print('found fpr=%.3f  LP+=%.3f  thr=%.3f'%(x,y,fpr_cut))
            break

        ax1.plot(fpr, tpr, label='ROC',color='seagreen' )
        ax1.plot([0, 1], [0, 1], 'k--', label='coin flip')
        ax1.axvline(x=x,linewidth=1, color='blue')
        ax1.set(xlabel='False Positive Rate',ylabel='True Positive Rate',title='ROC , area = %0.3f' % roc_auc)
        ax1.legend(loc='lower right',title=name+'-data')
        ax1.grid(color='brown', linestyle='--',which='both')

        ax2.plot(fpr,LRP, label='ROC', color='teal')
        ax2.plot([0, 1], [1, 1], 'k--',label='coin flip')
        ax2.set(ylabel='Pos. Likelih. Ratio',xlabel='False Positive Rate',title='LR+(FPR=%.2f)=%.1f'%(x,y))
        ax2.set_xlim([0,.2])

        ax2.axvline(x=x,linewidth=1, color='blue')
        ax2.legend(loc='upper right')
        ax2.grid(color='brown', linestyle='--',which='both')

        print('AUC: %f' % roc_auc)

