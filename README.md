# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* classification genome based on sampled protein set
* More deatls are in Readme file 

### How do I get set up? ###

* Summary of set up
For testing on CPU source mySetup_nersc.source 
To run on Cori GPU only load module escori and the rest of setup is done inside  batchTrain.slr

* Configuration
Need to re-format raw fasta files as HD5 , split data into 9 segements.
Create 2 dirs: out/ data/
adjust data/content.genomePlenum.yaml  as needed,
next run format_GenomePlenum.py 
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact