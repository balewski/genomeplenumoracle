import numpy as np
import time,  os
import h5py
import ruamel.yaml  as yaml
import random
#...!...!....................
def read_oneGenome_fasta(fName,aminoacidSet, seqLenClip,myShuffle=True, maxProt=50000):
        '''Hugh: 
        it is unique name >1102507_contig_1_3 # 1731 # 2822 # -1 #         
        drop proteins <30 or >15k  - defined by protLenClip
        '''
        #print('read from ',fName,' seqLenClip=',seqLenClip,' maxProt=',maxProt)

        seqLenLo, seqLenHi=seqLenClip
        protDB=['none%d'%x for x in range(maxProt)  ]   # it should speed up staging

        fp = open(fName, 'r')
        protN=''
        cnt={'lo':0,'acc':0,'hi':0,'any':0}
        for line in fp:
            line = line.rstrip()
            if line[0]=='>':#  header: Name|Species|association
                cnt['any']+=1
                #print('L:',line)
                if cnt['acc']>= maxProt: break # enough data was collected
                if cnt['any'] %50000==0:
                    print('add ',protN,cnt)
                if len(protN) >0: #archive last protein
                    seqLen=len(seq)
                    if seqLen <=seqLenLo: #skip too short
                            cnt['lo']+=1
                    elif seqLen >seqLenHi: # skip too long sequences
                            cnt['hi']+=1
                            #  seq=seq[:seqLenHi]
                    else: # accept those sequences
                        fastaIdx=cnt['any']
                        protDB[cnt['acc']]= [ protN, seqLen, fastaIdx, seq]
                        cnt['acc']+=1

                protN='none1'; seq='none2'

                assert 'contig' in line
                line2=line[1:].split('#')
                #print('line2=',line2)
                line3=[x.strip() for x in line2]
                #print('line3=',line3)
                protN=':'.join(line3[:4])
                seq=''
            else:    # - - - - amino acid codes  are valid
                #if '*' in line: line=line.replace('*','') # 
                #print('sss',set(line)-aminoacidSet)
                #print(line)
 
                assert aminoacidSet.issuperset(set(line))
                assert len(protN)>0
                seq+=  line
        fp.close()

        protDB=protDB[:cnt['acc']] # clip  not used list elements

        #print('  read proteins' ,cnt,' out list len=',len(protDB))
        assert len(protDB)>1
        if myShuffle:   random.shuffle(protDB) # in place
        return protDB,cnt


#...!...!..................
def read_yaml(ymlFn):
        print('  read  yaml:',ymlFn,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.Loader)
        ymlFd.close()
        print(' done, size=%d'%len(bulk),'  elaT=%.1f sec'%(time.time() - start))
        return bulk

#...!...!....................
def procure_ocean(genomeDB):
        nGenTot=len(genomeDB)
        start=time.time()
        print('sample_ocean inp genomes:',nGenTot)
        ocean=[]
        for gName in genomeDB:
            oneGen= genomeDB[gName]
            numProt=len(oneGen)
            n_get=10+numProt//10 # random sample ~400 proteins per genome
            keepIdx=np.random.choice(numProt, size=n_get,replace=False)
            #print('kk',keepIdx)
            for i in keepIdx:
                ocean.append(oneGen[i])            
 
        print('%d genome sampled, collected numProt=%d '%( nGenTot,numProt ),' , elaT=%.1f sec'%(time.time() - start))
        print('end-ocean size',len(ocean))
        return np.array(ocean)


#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        start = time.time()
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1048576
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.2f MB'%xx,'  elaT=%.1f sec'%(time.time() - start))

#...!...!..................
def write_data_hdf5(dataD,outF):
        h5f = h5py.File(outF, 'w')
        print('save data as hdf5:',outF)
        for item in dataD:
           rec=dataD[item]
           h5f.create_dataset(item, data=rec)
        print('h5-write :',item, rec.shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB, frames=%d'%(xx,rec.shape[0]))

#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD

#...!...!..................
# alternative noise injections
#...!...!..................
def noise_algo2(protL):
    protL2=[]
    for seq0 in protL:
        seq1=seq0[1:-1] # skip M,* at both ends
        str_var = list(seq1)
        random.shuffle(str_var)
        seq2=seq0[0]+''.join(str_var)+seq0[-1]
        protL2.append(seq2)
        #print('aaaINP',seq0,len(seq0))
        #print('bbbOUT',seq2,len(seq2))
    return protL2

#...!...!..................
def noise_algo3456(rndFrac,protL,aminoL):
    #print('noise_algo3456, rndFrac=%f'%rndFrac)
    numAmino=len(aminoL)
    #print('aminoL=',aminoL,numAmino,'rndFrac=',rndFrac)

    protL2=[]
    for seq0 in protL:
        seq1=seq0[1:-1] # skip M,* at both ends
        str_var = list(seq1)
        str_len=len(seq1)
        n_rnd=int(str_len*rndFrac)
        #print('aaaINP',seq0,str_len,'n_rnd=',n_rnd)
        rndIdx=np.random.choice(str_len, size=n_rnd,replace=False)
        #print('rndIdx=',rndIdx)
        rnd_aminoL=list(np.random.choice(aminoL,size=n_rnd,replace=True))
        #print('rnd_aminoL=',rnd_aminoL,type(rnd_aminoL),len(rnd_aminoL))

        for i in range(n_rnd):
            idx=rndIdx[i]
            str_var[idx]=rnd_aminoL[i]

        seq2=seq0[0]+''.join(str_var)+seq0[-1]
        #print('bbbOUT',seq2,len(seq2))
        protL2.append(seq2)
    return protL2
    
#...!...!..................
def noise_algo7(nPart,protL):
    #print('noise_algo7, nPart=%d '%(nPart))
    protL2=[]
    for seq0 in protL:
        seq1=seq0[1:-1] # skip M,* at both ends
        str_len=len(seq1)
        rndIdx=sorted(np.random.choice(str_len, size=nPart,replace=False))
        #print('aaaINP',seq0,str_len+2,'\nrndIdx=',rndIdx)
        i0=rndIdx[0]
        partL=[seq1[:i0]] #1st part
        for i in rndIdx[1:]:
            partL.append(seq1[i0:i])
            i0=i
        partL.append(seq1[i0:]) # last parte
        #print('inp',partL)
        random.shuffle(partL)
        #print('out',partL)
        seq2=seq0[0]+''.join(partL)+seq0[-1]
        l2=len(seq2)
        #print('bbbOUT',seq2,l2)
        protL2.append(seq2)
    return protL2
'''    
#################### Now make the data generator threadsafe ####################
import threading
class threadsafe_iter:
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self): # Py3
        return next(self.it)
    

def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_iter(f(*a, **kw))
    return g

@threadsafe_generator
'''
#...!...!..................
def genomePlenum_inputGeneratorFnc2(data,deep): 
    # always yields ballanced, randomized batch
    ocean=data['ocean'] 
    genomeD=data['genome']
    genomeL=list(genomeD.keys())
    mxGenomes=len(genomeL)
    hparD=deep.hparams
    metaD=deep.metaD
    aminoL=list(deep.metaD['aminoacids'][:-1]) # skip '*'
 
    oneHotBase=deep.oneHotBase
    batch_size=deep.hparams['batch_size']
    algoIdx=deep.algoIdx
    if hparD['shuffle']: random.shuffle(genomeL)

    print('IGFC2: ocean  size:',ocean.shape,' mxGenomes=',mxGenomes,' algoIdx=',algoIdx)
    num_inp=hparD['num_inputs']
    num_amino=hparD['num_amino']    
    print('IGFC2: num_inp=%d, num_amino=%d, batch_size=%d shuffle=%d'%(num_inp,num_amino,batch_size,hparD['shuffle']))
    assert batch_size%2==0 # needed for balanced batch
    bs=batch_size//2

    def amino2hot(aminoSeq):
        #print('1h1h1',type(aminoSeq),aminoSeq)
        hot2D=[ oneHotBase[y] for y in aminoSeq]
        return np.array(hot2D).astype(np.float32)

    def get_subString(inpSeq):
        nTot=len(inpSeq)
        jMx=nTot-num_amino
        if  jMx>=0 : # pick & trim
            jOff=np.random.randint(jMx+1)
            #print('take subset from ',jMx, jOff)   
            outSeq=inpSeq[jOff:jOff+num_amino]
        else: # append
            outSeq=inpSeq+metaD['padChar']*(-jMx)
        #print('outSeq',outSeq,len(outSeq), type(outSeq))
        assert len(outSeq)==num_amino
        #return outSeq  # this would be letters for amino acid sequence
        return amino2hot(outSeq)  # this is 1-hot encoded amino acid seq.

    def get_protSubSeqL_from_genome(genome):
        # grab num_inp random proteins from the  this genome
        idxL=np.random.choice(genome.shape[0],num_inp , replace=False)
        #print(' chose from %d idxL:'%genome.shape[0],idxL,type(genome),genome.shape,type(genome[0]))
        protL=genome[idxL] # select sub-list by index

        if algoIdx==2: 
             protL=noise_algo2(protL)
        # Reaplce xx aminoacids at random
        if algoIdx==3: 
             protL=noise_algo3456(0.2,protL,aminoL)
        if algoIdx==4: 
             protL=noise_algo3456(0.4,protL,aminoL)
        if algoIdx==5: 
             protL=noise_algo3456(0.6,protL,aminoL)
        if algoIdx==6: 
             protL=noise_algo3456(0.8,protL,aminoL)

        if algoIdx==7: # Shiffle 20 random subsegments
             protL=noise_algo7(20,protL)
        
        #[print('ex',x, len(x),type(x)) for  x in protL ]
        protL2=[ get_subString(x) for x in protL ]
        #print('ppp',len(protL2),protL2[0])
        return protL2
   
    # loop indefinitely
    j=0 # for traciking index for complete genomes
    while True:
        labL=[0]*bs + [1]*bs  # unrandomized labels
        protL=[]; 
        #print('is ocean',type(ocean))          
        for _ in range(bs):
            protL.append(get_protSubSeqL_from_genome( ocean))
        #print('is complete genome')
        for _ in range(bs):
           if j>=mxGenomes-1-bs: j=0 # reset pointer if not enough for the next batch
           genL=genomeL[j:j+bs]
           j+=bs
           #print('ge:',j,genL)
           for gName in genL:
                protL.append(get_protSubSeqL_from_genome( genomeD[gName]) )

        idxL=[i for i in range(batch_size)]
        protA=np.array(protL)
        labA=np.array(labL)
        #print('see 1-hot for',len(labL),labL,protA.shape,protA.nbytes,protA[0][0].nbytes,len(protA[0][0]),protA[0][0],type(protA[0][0]) )
        if hparD['shuffle']: 
           random.shuffle(idxL)
           protA=protA[idxL]
           labA=labA[idxL]
           #print('shuffled labels',labA)
        
        X=[ protA[:,i] for i in range(num_inp) ]
        #X=np.array(X)
        Y=labA
        yield (X,Y)
