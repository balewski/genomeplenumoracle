#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
#set -e ;    #  bash exits if any statement returns a non-true return value
#set -o errexit ;  # exit if any statement returns a non-true return value

gloProcIdx=${SLURM_PROCID}
locProcIdx=${SLURM_LOCALID}
wrkDir0=$1
echo "driveTrain procIdx glob=$gloProcIdx loc=$locProcIdx started "`hostname` 
echo server gprocId=$gloProcIdx pack sees nodes $SLURM_NODELIST

export OMP_NUM_THREADS=1
CUDA_VISIBLE_DEVICES=$locProcIdx

kfoldOff=$(( ${gloProcIdx} % 8 ))
algoIdx=$(( ${gloProcIdx} % 7 ))

# modify algoIdx
#if   [ $algoIdx -eq 1 ]; then
#    algoIdx=3
if [ $algoIdx -ge 1 ]; then
    algoIdx=$[ $algoIdx +1 ]
fi

dataDir=data2
events=0
epochs=150
design=perm1
srcDir=`pwd`
codeList="*.py  batchTrainNode.slr  hpar_genomePlenum-${design}.yaml" 
# attic: 

date
echo train-`hostname`-globIdx-$gloProcIdx start-jid=${SLURM_JOBID} algoIdx=$algoIdx
echo SLURM_CLUSTER_NAME=$SLURM_CLUSTER_NAME kfoldOff=$kfoldOff

#srun env|grep SLURM
module load tensorflow/gpu-1.13.1-py36
module list

wrkDir=$wrkDir0/algo${algoIdx}/${gloProcIdx}
echo globIdx-$gloProcIdx use wrkDir=$wrkDir
mkdir -p $wrkDir/out
cp -rp $codeList  $wrkDir
ln -s  $srcDir/$dataDir ${wrkDir}/data
cd  $wrkDir
echo PWD=`pwd` dataDir=`ls -l data`
if [ $locProcIdx -eq 0 ]; then
   echo "I am master on host:"`hostname`  
   nvidia-smi -l 2 >&L.smi& 
   ( sleep 180; date; free -g; top ibn1; pstree -p balewski) >&T.3m &
   ( sleep 300; date; free -g; top ibn1; pstree -p balewski) >&T.5m &
   ( sleep 1200; date;  free -g; top ibn1; pstree -p balewski) >&T.20m &
   ( sleep 3600; date;  free -g; top ibn1; pstree -p balewski) >&T.60m &
fi


# hyper-parm variation
#./genHPar_CellHH.py --nInpFeat 7000 --nOutFeat  10  --outPath ./ 
# design=${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}
# mv hpar_cellRegr_experiment.yaml  hpar_cellRegr_${design}.yaml; ls -l

# to extend training for another round
# seedWeights=../../138877/${arrIdx}/out
# echo seedWeights=$seedWeights
# ls -l $seedWeights/*weights_best.h5

echo use design=$design
startSkew=20 # (seconds), random delay for task
nsleep=$(($RANDOM % $startSkew))
echo nsleep=$nsleep
sleep $nsleep

# train from scratch
python -u ./train_GenomePlenum.py    --noXterm  --design $design  --algoIdx ${algoIdx} --verbosity 0  --kfoldOffset $kfoldOff --checkPt  --epochs $epochs >& train.log

# attic: --seedWeights $seedWeights  --nCpu 2

echo train-done-`date`-start-predict
sleep 10
python -u ./predict_GenomePlenum.py      --noXterm  --design $design   --algoIdx ${algoIdx} >&pred.log

echo predict-done-`date`

sleep 10
echo 'done  on '`hostname`' '`date` ' globIdx-'$gloProcIdx
