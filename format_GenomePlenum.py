#!/usr/bin/env python
"""  read raw input data for positive examples
write data as HD5 files, do  k-fold split, do metaD
Note, negative examples will be madeup in the streamer, not here
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_GenomePlenum import Plotter_GenomePlenum
from Deep_GenomePlenum import Deep_GenomePlenum
from Util_Func import write_yaml

import argparse,os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-d", "--dataPath",help="output path",  default='data')
    parser.add_argument("-o", "--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--events", type=int, default=10,
                        help="max events for role, use 0 for all")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='genomePlenum'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    args.arrIdx=0 # for plotter, not needed here
    assert os.path.isdir(args.outPath) # check it now to avoid lost time
    assert os.path.isdir(args.dataPath) # check it now to avoid lost time
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_GenomePlenum.formater(args)
gra=Plotter_GenomePlenum(args,deep.metaD )

genomeDB=deep.import_all_fasta()

for xx in genomeDB:
    oneGenome=genomeDB[xx]
    break
a1=0; a2='aa'
for  protN, seqLen, fastaIdx, seq in oneGenome:
    #print(x)
    if  a1< seqLen:
        a1=seqLen
        a2=seq
print(a2,a1)
ok66
splitD=deep.split_genomes(genomeDB)
write_yaml(deep.metaD,deep.metaF)

# this will take a long time ...
deep.save_fasta_hdf5(genomeDB,splitD)

print('\n\n test read of hd5 ...')
inpF='data/genomePlen_seg1.hd5'
import  time
start=time.time()
oneSegD=deep.read_oneSeg_hdf5(inpF)
print(' 2D is OK, blob len:',len(oneSegD),', elaT=%.1f sec'%(time.time() - start))
print('oneSeg keys:',inpF)
for gName in oneSegD.keys():
    print('key,len:',gName, len(oneSegD[gName]['amino_len']))

exit(0)


gra.plot_raw_fasta(genomeDB,'isolate amino seq')
gra.display_all(args,'form')


