#!/usr/bin/env python
""" read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_GenomePlenum import Plotter_GenomePlenum
from Deep_GenomePlenum import Deep_GenomePlenum

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign', default='perm1',
                        choices=['perm1'], help=" model design of the network")
    parser.add_argument( "--algoIdx", type=int, default=0,
                        help="alters how algo works, 0 is default algo")

    parser.add_argument("-d", "--dataPath",help="output path",  default='data')
    parser.add_argument("--seedModel",default='same',
                        help="trained model and weights")

    parser.add_argument("-o", "--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument("-b", "--batch_size", type=int, default=None,
                        help="fit batch_size, None: use hpar value")
    parser.add_argument("-s", "--steps", type=int, default=None,
                        help="fit steps, None: use hpar value")

    parser.add_argument('-X', "--noXterm", dest='noXterm',
                        action='store_true', default=False,
                        help="disable X-term for batch mode")
    args = parser.parse_args()
    args.prjName='genomePlenum'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#...!...!..................
from permutational_layer import move_item #move_item(a_list, from_idx, to_idx)

#...!...!..................
def XXtest_permutability(X):
    nPt=len(X)
    for j in range(1):
        XL=[X[i][j:j+1] for i in range(nPt) ] 
        print('pp',j,nPt,XL[0].shape)
        print('vv0', XL[:3])
        Yprob = deep.model.predict(XL).flatten()
        print('Yprob',Yprob)
        move_item(XL, 0,2)
        print('vv1', XL[:3])
        Yprob = deep.model.predict(XL).flatten()
        print('Yprob',Yprob)
 
            
        index0=0
        inputs_permuted = [x for x in range(nPt)]  
        print('init inputs_permuted=',inputs_permuted)
        for index1 in range(nPt):
            move_item(inputs_permuted, index0, index1)
            print('index1=',index1,' inputs_permuted=',inputs_permuted)
            move_item(XL, index0, index1)
            #print('vv1', XL[:3])
            Yprob = deep.model.predict(XL).flatten()
            print('Yprob',Yprob)



#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_GenomePlenum.predictor(args)
gra=Plotter_GenomePlenum(args,deep.metaD)
dom='test'
#dom='val'
deep.load_segmented_input(dom)

if args.seedModel=='same' :  args.seedModel=args.outPath
deep.load_model_full(args.seedModel)
print('M: predicting, batch_size=%d dom=%s'%(deep.hparams['batch_size'],dom))
Yhot,Yscore=deep.make_predictions(dom)
gra.plot_labeled_scores(Yhot,Yscore,dom)
gra.plot_AUC(Yhot,Yscore,dom,args)

#deep.load_weights('weights_best') 
#deep.make_prediction(dom)

gra.display_all(args,'predict')



