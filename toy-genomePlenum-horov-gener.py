#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
https://www.pyimagesearch.com/2018/12/24/how-to-use-keras-fit-and-fit_generator-a-hands-on-tutorial/

 example of simple FC+horovod +generator(file)
 trains
saves model
reads model
predicts
uses generator for input

See examples of data generators:
https://github.com/keras-team/keras/issues/7729

****Run on Cori GPUs
ssh cori 
module load escori 
module load cuda
module load tensorflow/gpu-1.13.1-py36 

salloc -C gpu -n 80 --gres=gpu:8 -Adasrepo -t4:00:00

# run training w/o GPU-load monitor

srun  -n 1 bash -c '  python -u  toy-genePlenum-horov-gener.py'
srun  -n 6 bash -c '  python -u  toy-genePlenum-horov-gener.py -G'
# run with 1 GPU-load monitor
srun  -n 2   bash -c ' hostname & if [ $SLURM_PROCID -eq 0 ] ; then  nvidia-smi -l 2 >&L.smi ; fi &  python -u toy-genePlenum-horov-gener.py'

srun bash -c '  nvidia-smi '

******  Run on Cori CPUs , all on 1 node as single task
ssh cori
module load tensorflow/intel-1.12.0-py36
toy-genePlenum-CPUorGPU.py  --noHorovod     --steps 5

If you see: HD5 OSError: Unable to create file (unable to lock file, errno = 524..
 export HDF5_USE_FILE_LOCKING=FALSE 

'''

import socket  # for hostname
import os, time, sys
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import backend as K
from tensorflow.keras.layers import  Dense, Input, maximum,Conv1D,MaxPool1D,Flatten,Dropout
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model
import horovod.tensorflow.keras as hvd 
import h5py

sys.path.append(os.path.abspath("/global/homes/b/balewski/digitalMind/keras/superkeras-chanchana/"))
from permutational_layer import PairwiseModel, PermutationalEncoder,PermutationalLayer, move_item
from layers import repeat_layers

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

import argparse

parser = argparse.ArgumentParser()
parser.add_argument( "--noHorovod", dest='useHorovod',  action='store_false',
                     default=True, help="disable Horovod to run on CPU")
parser.add_argument( "-G","--useGen", dest='useGenerator',
                     action='store_true', default=False,
                    help=" create data as file, use generator to read & feed training")
parser.add_argument("-b", "--batch_size", type=int, default=4096,
                        help="training batch_size")
parser.add_argument("-e", "--epochs", type=int, default=16,
                        help="training epochs")
parser.add_argument("-s", "--steps", type=int, default=80,
                        help="steps per training epochs")
parser.add_argument("--seedWeights",default=None,
                    help="seed weights only, after model is created")
parser.add_argument("--lr", type=float, default=1.,
                    help="learning rate for Adadelta optimizer")
parser.add_argument("--dropFrac", type=float, default=0.02,
                    help="drop fraction at all layers")
parser.add_argument("-d","--dataPath",default='data/',help="path to input")
parser.add_argument("-o", "--outPath",default='out/',
                    help="output path for plots and tables")


#...!...!..................
def genePlenum_inputGeneratorFnc(inputPath, bs):    
    # open the HD5 file for reading
    blob=read_data_hdf5(inputPath)
    j=0
    mxJ=blob['Y'].shape[0]
    protSetSize=blob['X'].shape[1]
    # loop indefinitely
    while True:
        if j>=mxJ-1-bs: j=0 # reset pointer if not enough for next batch
        Xall=blob['X'][j:j+bs]
        Y=blob['Y'][j:j+bs]
        j+=bs
        X=[ Xall[:,i] for i in range(protSetSize) ]
        yield (X,Y)


#...!...!..................
def write_data_hdf5(dataD,outF):
        h5f = h5py.File(outF, 'w')
        print('save data as hdf5:',outF)
        for item in dataD:
           rec=dataD[item]
           h5f.create_dataset(item, data=rec)
           print('h5-write :',item, rec.shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%(xx))

#...!...!..................
def read_data_hdf5(inpF):
        print('\nread data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD


#...!...!..................
def build_permu_model(one_shape,num_inputs,dropFrac ):
        print('\nPER step-A - - - - - - - -')
        print('PER: 1 one_shape=',one_shape,' num_inputs=',num_inputs)
        model1 = PairwiseModel(
                one_shape, repeat_layers(Conv1D, [32], [3], name="pairCNN1D"), name="pairwise_op"
            )
        
        h=model1.layers[-1].output # the last layer
        kernel = 3
        pool_len = 3 # how much time_bins get reduced per pooling
        cnnDim=[66,88]
        numCnn=len(cnnDim)
        print(' cnnDims:',cnnDim)
        for i in range(numCnn):
            dim=cnnDim[i]
            h= Conv1D(dim,kernel,activation='relu', padding='valid',name='cnn%d_d%d_k%d'%(i,dim,kernel))(h)
            h= MaxPool1D(pool_size=pool_len, name='pool_%d'%(i))(h)
            print('cnn',i,h.get_shape())
 
        h=Flatten(name='to_1d0')(h)
        pairwise_op = Model(model1.inputs, outputs=h)

        mod=pairwise_op        
        print('PER: PairOP+JanPool  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))
        mod.summary()

        if verb>1:
            outF='pairwise_op.graph.svg'
            plot_model(mod, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M: saved1 ',outF)

        print('\nPER step-B - - - - - - - ')
        encoderRow_op = PermutationalEncoder(pairwise_op,num_inputs , name="encoder_row",encode_identical=False) # skip auto-correlation terms

        mod=encoderRow_op
        print('PER: EncoderRow  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if verb>2:
            mod.summary()
            outF='encoderRow_op.graph.svg'
            plot_model(encoderRow_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved2 ',outF)


        print('\nPER step-C - - - - - - ')
        perm_layer = PermutationalLayer(encoderRow_op, name="permutational_layer", pooling=maximum)

        mod=perm_layer
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if verb>2:
            mod.summary()
            outF='permLyr.graph.svg'
            plot_model(perm_layer, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved3 ',outF)

        print('\nPER step-D - - - - - - \n add few common FC layers --> sigmoid')
        # append non-permutational  layers to the model
        model2=perm_layer
        h=model2.layers[-1].output # the last layer

        print('in FC comm=>',h.get_shape())
        h= Dropout(dropFrac)(h)
        h= Dense(30, activation='relu',name='common1')(h)
        h= Dropout(dropFrac)(h)
        h= Dense(10, activation='relu',name='common2')(h)
        h= Dropout(dropFrac)(h)
        outputs= Dense(1, activation='sigmoid',name='sigmoid')(h)
        model3 = Model(model2.inputs, outputs)

        mod=model3
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if verb>0:
            mod.summary()
            outF='fullModel.graph.svg'
            plot_model(model2, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved4 ',outF)

        return model3
        
#...!...!..................


#=================================
#=================================
#  M A I N 
#=================================
#=================================

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
for xx in [args.dataPath,args.outPath]:
        assert  os.path.exists(xx)

if args.useHorovod:
    hvd.init()
    gpu_count= hvd.size()
    myRank= hvd.rank()
    print(' Horovod: initialize Horovod, myRank=%d of %d'%(myRank,gpu_count))
    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = str(hvd.local_rank())
    K.set_session(tf.Session(config=config))
else:
    gpu_count= 1
    myRank=0
    print('Horovod disabled, gpu_count=',gpu_count)
    
# genome code starts here 
protSetSize=10  #  num. of proteins pulled from a genome, to be permuted
aminoAlphabetSize=22  # the size of used alphabet
aminoSeqLen=500 #proteins: fix length of amino acids, randomly clip if too long 
#padChar='*'  #  if too short pad it with this char
#numProbes=20 # num. of repeats of pulling genome (not used here)

prot_shape=(aminoSeqLen,aminoAlphabetSize)  # 1-hot encoded single protein
#prot_shape=(aminoSeqLen,)  # 1-hot encoded single protein

batch_size=16*2
steps=200# use: 200 for GPU, 7 for CPU
num_eve=steps*batch_size
num_eve_val=num_eve//5
epochs=10
verb=0
if myRank>0 : verb==0

model= build_permu_model(prot_shape,protSetSize,args.dropFrac)

all_shape=(num_eve+num_eve_val,) + (protSetSize,) + prot_shape
print('\nM:  - - - - - - -generate data and train, num_eve=',num_eve,', Xall.shape=',all_shape)

Xall=np.random.normal(loc=0.3, scale=0.7, size=all_shape)
Yall=np.random.randint(2,size=all_shape[0])
# add signal to data
for i,y in enumerate(Yall):
    k=np.random.randint(protSetSize)
    val=10*(y-0.5)
    #print('i,y',i,y)
    j=np.random.randint(prot_shape[0])
    Xall[i][k][j][:]=val

# permu-model expects X as a list of num_inputs arrays of the shape=(num_eve,prot_shape)
X=[ Xall[:num_eve,i] for i in range(protSetSize) ]
Y=Yall[:num_eve]
Xval=[ Xall[num_eve:,i] for i in range(protSetSize) ]
Yval=Yall[num_eve:]
print('M:data created, Y shape',Y.shape,' Yval shape',Yval.shape, 'num_inputs=',protSetSize, 'one_shape=',prot_shape)

myCallbacks = []
LR=args.lr
if args.useHorovod:
    #LR=1 *gpu_count
    opt = keras.optimizers.Adadelta(lr=LR)
    #Adadelta.__init__(lr=1.0,rho=0.95,epsilon=None,decay=0.0,**kwargs)

    #LR/1000.; opt = keras.optimizers.Adam(lr=LR)  # explodes for nGpu>1
    print('Horovod: adjust learning rate to',LR)
    print(opt)

    # Horovod: add Horovod Distributed Optimizer.
    myOpt = hvd.DistributedOptimizer(opt)
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    myCallbacks.append(hvd.callbacks.BroadcastGlobalVariablesCallback(0))
else:
    myOpt = keras.optimizers.Adadelta(lr=LR)

print('compile rank=%d LR=%.2f ...'%(myRank,LR))
model.compile(optimizer=myOpt, loss='binary_crossentropy', metrics=['accuracy'])

#if myRank == 0: model.summary() # will print

if args.seedWeights=='same' :  args.seedWeights=args.outPath
if args.seedWeights:
    inpF5=args.seedWeights+'/jan-FC-ok0.model.h5' # val_loss: 0.25
    #inpF5=args.seedWeights+'/jan-FC-ok1.model.h5' # val_loss: 0.0138
    print(myRank,'restore weights from HDF5:',inpF5)
    model.load_weights(inpF5) 
    

# Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
#if hvd.rank() == 0:
#    callbacks.append(keras.callbacks.ModelCheckpoint(outPath+'/checkpoint-{epoch}.h5'))


if args.useGenerator:
    outF=args.dataPath+'geneFake_%s_rank%d.h5'%('train',myRank)
    dataD={'X':Xall[:num_eve],'Y':Yall[:num_eve]}
    write_data_hdf5(dataD,outF)
    outF=args.dataPath+'geneFake_%s_rank%d.h5'%('val',myRank)
    dataD={'X':Xall[num_eve:],'Y':Yall[num_eve:]}
    write_data_hdf5(dataD,outF)

print('fit...rank=',myRank)
startTm = time.time()

if not args.useGenerator:
    model.fit(X,Y, batch_size=batch_size, callbacks=myCallbacks, epochs=epochs, verbose=1, validation_data=(Xval, Yval))
else:
    assert steps>=5
    inpF=args.dataPath+'/geneFake_%s_rank%d.h5'%('train',myRank)
    trainGen = genePlenum_inputGeneratorFnc(inpF, batch_size)
    inpF=args.dataPath+'/geneFake_%s_rank%d.h5'%('val',myRank)
    valGen = genePlenum_inputGeneratorFnc(inpF, batch_size)

    # test one of generators
    xx,yy =next(valGen)  # X=[ np(batch,one_shape) ]_item
    one_shape=xx[0][0].shape  
    num_inputs=len(xx)
    assert batch_size == xx[0].shape[0]
    print('input generator activated, X num_inputse=',num_inputs,' one_shape=',one_shape)

    model.fit_generator(trainGen, callbacks=myCallbacks, epochs=epochs, steps_per_epoch=steps ,validation_data=valGen, validation_steps=steps//5)


fitTime=time.time() - startTm
print('M:fit done  elaT=%.1f sec, BS=%d epochs=%d'%(fitTime,batch_size,epochs))

if myRank > 0: exit(0)
# no need to repeat it for all ranks

print("-----------  Save model as HD5 -----------")
outF=args.outPath+'jan-FC.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from HD5 -----------")
model5=load_model(outF)

print('M: fit done, predicting')

if args.useGenerator:
    Zval=model5.predict_generator(valGen,steps=3)
else:
    Zval=model5.predict(Xval)
print('M: done, Z:',Zval.shape,Yval.shape)
ZmY=Zval-Yval
# now compute MSE
sqErr=np.power(ZmY,2)
print('ss',sqErr.shape)
sqErr=sqErr.mean(axis=1)
print('ss',sqErr.shape)
msqErr=sqErr.mean()
ssqErr=sqErr.std()
print('computed segment MSE mean=%.4f +/- %.4f'%(msqErr,ssqErr))



