#!/usr/bin/env python

'''
Toy ML-model  for binary classification of genome: INPUT is a set of genes. 

  using Permutational Layer
Modiffied from
https://github.com/off99555/superkeras/blob/master/permutational_layer.py
Oryginal by:  Chanchana Sornsoontorn (off99555)
Run on Cori CPU:

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time,sys
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np

import tensorflow as tf
from tensorflow.keras.layers import  Dense, Input, maximum,Conv1D,MaxPool1D,Flatten
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model

sys.path.append(os.path.abspath("/global/homes/b/balewski/digitalMind/keras/superkeras-chanchana/"))
from permutational_layer import PairwiseModel, PermutationalEncoder,PermutationalLayer, move_item
from layers import repeat_layers

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))


#...!...!..................
def build_permu_model(one_shape,num_inputs ):
        print('\nPER step-A - - - - - - - -')
        print('PER: 1 point_shape=',one_shape,' num_inputs=',num_inputs)
        pairwise_op = PairwiseModel(
                one_shape, repeat_layers(Conv1D, [32], [3], name="pairCNN1D"), name="pairwise_op"
            )
        
        model1=pairwise_op
        h=model1.layers[-1].output # the last layer
        kernel = 3
        pool_len = 3 # how much time_bins get reduced per pooling
        cnnDim=[66,88]
        numCnn=len(cnnDim)
        print(' cnnDims:',cnnDim)
        for i in range(numCnn):
            dim=cnnDim[i]
            h= Conv1D(dim,kernel,activation='relu', padding='valid',name='cnn%d_d%d_k%d'%(i,dim,kernel))(h)
            h= MaxPool1D(pool_size=pool_len, name='pool_%d'%(i))(h)
            print('cnn',i,h.get_shape())
 
        h=Flatten(name='to_1d0')(h)
        pairwise_op = Model(model1.inputs, h)

        mod=pairwise_op        
        print('PER: PairOP+JanPool  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))
        if verb>1:
            mod.summary()
            outF='pairwise_op.graph.svg'
            plot_model(pairwise_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M: saved1 ',outF)

        print('\nPER step-B - - - - - - - ')
        encoderRow_op = PermutationalEncoder(pairwise_op,num_inputs , name="encoder_row",encode_identical=False) # skip auto-correlation terms

        mod=encoderRow_op
        print('PER: EncoderRow  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if verb>2:
            mod.summary()
            outF='encoderRow_op.graph.svg'
            plot_model(encoderRow_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved2 ',outF)


        print('\nPER step-C - - - - - - ')
        perm_layer = PermutationalLayer(encoderRow_op, name="permutational_layer", pooling=maximum)

        mod=perm_layer
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if verb>2:
            mod.summary()
            outF='permLyr.graph.svg'
            plot_model(perm_layer, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved3 ',outF)

        print('\nPER step-D - - - - - - \n add few common FC layers --> sigmoid')
        # append non-permutational  layers to the model
        model1=perm_layer
        h=model1.layers[-1].output # the last layer

        print('in FC comm=>',h.get_shape())

        h= Dense(30, activation='relu',name='common1')(h)
        h= Dense(10, activation='relu',name='common2')(h)
        outputs= Dense(1, activation='sigmoid',name='sigmoid')(h)
        model2 = Model(model1.inputs, outputs)

        mod=model2
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))
        mod.summary()

        if verb>1:
            outF='fullModel.graph.svg'
            plot_model(model2, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved4 ',outF)

        return model2
        
#...!...!..................





#=================================
#=================================
#  M A I N 
#=================================
#=================================
protSetSize=80  #  num. of proteins pulled from a genome, to be permuted
aminoSetSize=22  # the size of used alphabet
aminoSeqLen=500 #proteins: fix length of amino acids, randomly clip if too long 
padCahr='*'  #  if too short pad it with this char
#numProbes=20 # num. of repeats of pulling genome (not used here)

prot_shape=(aminoSeqLen,aminoSetSize)  # 1-hot encoded single protein

batch_size=16
steps=7
num_eve=steps*batch_size
epochs=10
verb=0

model= build_permu_model(prot_shape,protSetSize)

#''' fit-starts
print('\nM:  - - - - - - - - generate data and train, num_eve=',num_eve)

# Expects X as a list of num_inputs arrays of shape=(num_eve,one_shape)
X=[np.random.normal(loc=0.3, scale=0.7, size=(num_eve,prot_shape[0],prot_shape[1])) for i in range(protSetSize) ]
Y=sg=np.random.randint(2,size=num_eve)
print('M: Y shape',Y.shape, 'num_inputs=',protSetSize, 'one_shape=',prot_shape)

'''
# add signal to data
for i,y in enumerate(Y):
    k=np.random.randint(protSetSize)
    val=10*(k-0.5)
    #print('i,y',i,y)
    j=np.random.randint(prot_shape[0])
    X[i][j][k]=val
'''

# We compile the model , takes long time for large num_inputs
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy']) 
print('M: compiled')
model.fit(X,Y, batch_size=batch_size, epochs=epochs,verbose=1) 

# procure shorter version of inputX 
nPred=20
X2=[ X[j][:nPred] for j in range(protSetSize) ]
Y2=Y[:nPred]

Z2=model.predict(X2)
print("M: OutputFit Features")
for z,u in zip(Z2,Y2): 
   pr=z>0.5
   print('pred=',pr,'truth=',u,'diff=',pr-u)
#''' # end-fit


print("-----------  Save model as hd5 -----------")
outF='perm1.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from YAML -----------")
model1b=load_model(outF)


print('M:  predicting-1b')
Z=model1b.predict(X2)
print("M: Output5 Features")
for pred in Z: print(pred)

