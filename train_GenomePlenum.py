#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5

****Run on Cori GPUs
ssh cori 
module load escori 
module load cuda
module load tensorflow/gpu-1.13.1-py36 

salloc -C gpu -n 80 --gres=gpu:8 -Adasrepo -t4:00:00
unset XDG_RUNTIME_DIR

srun  -n 1 bash -c '  python -u  train_GenomePlenum.py'

# run with 1 GPU-load monitor
srun  -n 1   bash -c ' hostname & if [ $SLURM_PROCID -eq 0 ] ; then  nvidia-smi -l 2 >&L.smi ; fi &  python -u  train_GenomePlenum.py'

modify qos set maxjobsperuser=2 maxtresperjob=node=2 minpriothresh=-1 preempt='gpu_preempt' where name="gpu"
modify qos set maxjobsperuser=2 maxtresperjob=node=18 minpriothresh=-1 where name="gpu_preempt"
modify qos set maxjobsperuser=1 maxtresperjob=gpu=1 minpriothresh=-1 preempt='gpu_preempt' where name="gpu_high"

Exmplanation:  
For jobs requesting <= 4 h, you can have up to 2 nodes per job = 16 GPUs, or 32 GPUs spread across 2 jobs. If you request > 4 h you have access to all 18 nodes within a single job
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_GenomePlenum import Plotter_GenomePlenum
from Deep_GenomePlenum import Deep_GenomePlenum
from Util_Func import genomePlenum_inputGeneratorFnc2, write_yaml
import tensorflow as tf

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',  default='perm1',
                        choices=['perm1'], help=" model design of the network")
    parser.add_argument( "--algoIdx", type=int, default=0,
                        help="alters how algo works, 0 is default algo")

    parser.add_argument("-d", "--dataPath",help="output path",  default='data')
    parser.add_argument("-o", "--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-e", "--epochs", type=int, default=3,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=None,
                        help="fit batch_size, None: use hpar value")
    parser.add_argument("-s", "--steps", type=int, default=None,
                        help="fit steps, None: use hpar value")
    parser.add_argument("--dropFrac", type=float, default=None,
                        help="drop fraction at all layers")
    parser.add_argument( "-p","--earlyStop", type=int,
                         dest='earlyStopPatience', default=10,
                         help="early stop:  epochs w/o improvement (aka patience), 0=off")
 
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLR", dest='reduceLRPatience', type=int, default=5,help="reduce learning at plateau, patience")

    parser.add_argument('-X', "--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disables X-term for batch mode")

    args = parser.parse_args()
    args.prjName='genomePlenum'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_GenomePlenum.trainer(args)
gra=Plotter_GenomePlenum(args,deep.metaD)
# we load splitted positive examples as strings. The negative examples and 1-hot encodding are done by the generator
deep.load_segmented_input('train')
deep.load_segmented_input('val')

trainGen = genomePlenum_inputGeneratorFnc2(deep.data['train'],deep)
valGen = genomePlenum_inputGeneratorFnc2(deep.data['val'],deep)
# test one of generators
xx,yy =valGen.__next__()  # X=[ np(batch,one_shape) ]_item
#xx,yy =next(valGen)  # X=[ np(batch,one_shape) ]_item

one_shape=xx[0][0].shape
num_inputs=len(xx)


#deep.load_input_hdf5(['train','val'])
#ok22

#gra.plot_frames(deep.data['val'],6)

deep.build_permu_model(one_shape,num_inputs )

#gra.plot_model(deep)

deep.compile_model()
deep.train_model(trainGen,valGen)

deep.save_model_full() 
write_yaml(deep.sumRec, deep.outPath+'/'+deep.prjName2+'.sum.yaml')
gra.plot_train_history(deep,args)
dom='val'; deep.hparams['steps']=deep.hparams['steps']//5
print('M: predicting, batch_size=%d dom=%s'%(deep.hparams['batch_size'],dom))
Yhot,Yscore=deep.make_predictions(dom)
gra.plot_labeled_scores(Yhot,Yscore,dom,figId=-10)
gra.plot_AUC(Yhot,Yscore,dom,args,figId=-10)

gra.display_all(args,'train')    


